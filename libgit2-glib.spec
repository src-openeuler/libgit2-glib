%global libgit2_version 0.25.0
%global glib2_version 2.44.0

Name:           libgit2-glib
Version:        1.2.0
Release:        1
Summary:        Git library for GLib
License:        LGPLv2+
URL:            https://wiki.gnome.org/Projects/Libgit2-glib
Source0:        https://download.gnome.org/sources/libgit2-glib/1.2/libgit2-glib-%{version}.tar.xz

BuildRequires:  gcc gi-docgen pkgconfig(glib-2.0) >= %{glib2_version} pkgconfig(gobject-2.0) >= %{glib2_version} pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(pygobject-3.0) pkgconfig(libgit2) >= %{libgit2_version} libssh2-devel python3-devel vala meson
Requires:       glib2 >= 2.44.0 libgit2 >= 0.25.0 python3-gobject

%description
A glib wrapper library around the libgit2 git access library.

%package        devel
Summary:        Development files for libgit2-glib
Requires:       libgit2-glib = %{version}-%{release}

%description    devel
Libraries and header files for developing applications that use libgit2-glib.

%package        help
Summary:        Documentation for libgit2-glib-devel
Requires:       libgit2-glib-devel

%description    help
Documentation for libgit2-glib-devel.

%prep
%autosetup -n libgit2-glib-%{version} -p1

%build
%meson -Dgtk_doc=true -Dpython=true
%meson_build

%install
%meson_install

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc COPYING AUTHORS NEWS
%{_libdir}/{libgit2-glib-1.0.so.*,girepository-1.0/Ggit-1.0.typelib}
%{python3_sitearch}/gi/overrides/*

%files devel
%{_includedir}/libgit2-glib-1.0/
%{_libdir}/{libgit2-glib-1.0.so,pkgconfig/libgit2-glib-1.0.pc}
%{_datadir}/{gir-1.0/Ggit-1.0.gir,vala/}

%files help
%doc %{_datadir}/gtk-doc/

%changelog
* Mon Dec 18 2023 zhangkea <zhangkea@uniontech.com> - 1.2.0-1
- Update to 1.2.0

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 1.1.0-1
- Update to 1.1.0

* Wed Jun 30 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 0.99.0.1-1
- Upgrade to 0.99.0.1

* Tue Dec 31 2019 Ling Yang <lingyang2@huawei.com> - 0.27.8-2
- Package init
